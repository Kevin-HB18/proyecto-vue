import { createRouter, createWebHashHistory, createMemoryHistory, createWebHistory } from 'vue-router'
import HomeView from '../views/HomeView.vue'
import Personajes from "@/views/Personajes.vue"
import Lugares from "@/views/Lugares.vue"
import Episodios from "@/views/Episodios.vue"
const routes = [
  {
    path: '/',
    name: 'home',
    component: HomeView
  },
  {
    path: '/about',
    name: 'about',
    // route level code-splitting
    // this generates a separate chunk (about.[hash].js) for this route
    // which is lazy-loaded when the route is visited.
    component: () => import(/* webpackChunkName: "about" */ '../views/AboutView.vue')
  },
  {
    path: '/Personajes',
    name: 'personajes',
    component: Personajes 
  },
  {
    path: '/Lugares',
    name: 'lugares',
    component: Lugares 
  },
  {
    path: '/Episodios',
    name: 'episodios',
    component: Episodios 
  }
]

const router = createRouter({
  history: createWebHistory(),
  routes
})

export default router
